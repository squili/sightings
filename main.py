import pytz
import json
import os
import random
import requests
from datetime import datetime

cities = json.load(open('cities.json'))
sighted = []

utc_time = datetime.utcnow()

for city in cities:
    timezone = pytz.timezone(city['timezone'])
    now = timezone.fromutc(utc_time)
    if now.hour == 7 and now.minute == 27:
        sighted.append(city)

if len(sighted) == 0:
    print('no sightings')
    exit()

choice = sighted[random.choices(range(len(sighted)), map(lambda line: line['population'], sighted))[0]]

print(f'{choice["city"]}, {choice["country"]}')

requests.post(f'{os.environ["API_BASE"]}/notes/create', json={
    'text': f'Sighting in {choice["city"]}, {choice["country"]}\nhttps://www.openstreetmap.org/#map=12/{choice["gps"][1]}/{choice["gps"][0]}',
    'visibility': 'home',
}, headers={
    'Authorization': f'Bearer {os.environ["API_KEY"]}'
}).raise_for_status()
