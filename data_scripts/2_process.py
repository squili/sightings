import json
import tzfpy
import pytz

csv = open('query.csv').readlines()
labels = csv[0].strip().split(',')
csv = csv[1:]

before_lines = len(csv)

data = []

for line in csv:
    fields = line.split(',')
    line_data = {}
    try:
        for i, field in enumerate(fields):
            line_data[labels[i]] = field.strip()
    except IndexError:
        continue
    data.append(line_data)

# remove entries with empty data
data = list(filter(lambda line: all(map(lambda value: value != None, line.values())), data))
data = list(filter(lambda line: all(map(lambda value: len(value) != 0, line.values())), data))
# remove entries missing labels
data = list(filter(lambda line: all(map(lambda label: label in line, labels)), data))

for line in data:
    # strip wikidata urls
    line['city'] = line['city'][len('http://www.wikidata.org/entity/'):]
    line['country'] = line['country'][len('http://www.wikidata.org/entity/'):]
    # parse gps point
    pair = line['gps'][len('Point('):-1].split(' ')
    line['gps'] = (float(pair[0]), float(pair[1]))
    line['population'] = int(line['population'])

for i, line in enumerate(data):
    # deduplicate cities
    for o, check in enumerate(data[i+1:]):
        if check['city'] == line['city']:
            del data[i+o+1]

for i, line in enumerate(data):
    # deduplicate cities in countries
    for o, check in enumerate(data[i+1:]):
        if check['country'] == line['country'] and check['cityLabel'] == line['cityLabel']:
            del data[i+o+1]

for line in data:
    # resolve timezone
    line['timezone'] = tzfpy.get_tz(*line['gps'])
    # validate timezone
    pytz.timezone(line['timezone'])
    # switch tags and labels
    del line['city']
    del line['country']
    line['city'] = line['cityLabel']
    del line['cityLabel']
    line['country'] = line['countryLabel']
    del line['countryLabel']

json.dump(data, open('cities.json', 'w'), indent=2)

after_lines = len(data)

print(f'filtered {before_lines} entries to {after_lines} entries')
